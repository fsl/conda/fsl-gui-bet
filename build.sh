#!/usr/bin/env bash

export FSLDIR=$PREFIX
export FSLDEVDIR=$PREFIX

. $FSLDIR/etc/fslconf/fsl-devel.sh

mkdir -p $PREFIX/src/
cp -r $(pwd) $PREFIX/src/$PKG_NAME

# install gui deps
npm install
# build the gui
npm run build
# remove the stuff we dont need now
rm -r node_modules
# the npm run build command will create a dist folder with the bundled web files
mkdir -p $PREFIX/share/fsl/gui/bet
cp -r dist/* $PREFIX/share/fsl/gui/bet/

# Create entry point in $FSLDIR/bin/ using the FSL desktop launcher
${FSLDIR}/share/fsl/sbin/createFSLWrapper \
         -s $FSLDIR/bin/ -d $FSLDIR/bin/  \
         -f -r -a " --gui bet"            \
         fsl_gui_launcher=fsl_gui_bet
